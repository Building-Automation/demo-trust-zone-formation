package mainpackage;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Math;
import java.util.Random;

public class Main {

	public enum Shape {
		linear, random, square
	};

	public static int STEPS = 1;
	public static int NUMOFDEVS = 1000;
	public static int distanceNeighbor = 1;
	public static int area = 2000;
	public static int MAXNUMUMGEBUNG = 1;
	
	public static boolean firstrun = true;

	public static int ITERATIONS = 1;

	public static int avgNumberOfClustermembers = 0;
	

	static Random generator = new Random();

	public static void main(String[] args) throws IOException {

		int [][] values = new int[STEPS][ITERATIONS];
		int [] avgnumdev = new int[MAXNUMUMGEBUNG];
		int ninetyPercentReached = 0;

		for (int j = 0; j < STEPS; j++) {

			//NUMOFDEVS += 1000;

			Dev[] devices = new Dev[NUMOFDEVS];
			Dev[] cluster = new Dev[NUMOFDEVS];

			placeDevs(Shape.random, devices);
			// printLocations(devices);
			//savetoCSV("./Clusteringresults.csv", devices);

			long start, stop;
			long accu = 0;
			int umgebung = 100;
			//for(int umgebung = 0; umgebung < MAXNUMUMGEBUNG; umgebung++) {
				for (int i = 0; i < ITERATIONS; i++) {
					placeDevs(Shape.random, devices);
					start = System.nanoTime();
				//	calculateCluster(devices, cluster);
					calculateCluster1(devices, cluster, umgebung);
					stop = System.nanoTime();
					
				//	values[j][i] = (int) (stop - start)/1000000;
					
					accu += (stop - start);
			//	}
			
			accu /= ITERATIONS;
		//	System.out.println(NUMOFDEVS + "EDs - Avg. Execution Time [ms]: " + accu / 1000000);

			//System.out.println(
			//		NUMOFDEVS + "EDs - Avg. Number of Clustermembers: " + avgNumberOfClustermembers / ITERATIONS);
			
			//savetoCSV("./Timings.csv", values);
			
		//	savetoCSV("./cluster.csv", cluster);
			
		//	System.out.println("Umgebung;AvgNumClustermember");
			System.out.println("Umgebung: " + umgebung + " AvgNumClustermember: " + avgNumberOfClustermembers / ITERATIONS);
		//	avgnumdev[umgebung] = avgNumberOfClustermembers / ITERATIONS;
			
			
			if(firstrun) {
				if((avgNumberOfClustermembers / ITERATIONS) >= (0.9 * 0.6 * NUMOFDEVS)) {
					ninetyPercentReached = umgebung;
					firstrun = false;
				}
				}
			
			
			avgNumberOfClustermembers = 0;
			
			}
			
			
			System.out.println("Ninety Percent reached at Umgebung: " + ninetyPercentReached);
			
			savetoCSV("./Umgebung2000VsNumDevs1000Area2000RandomDreiecka.csv", avgnumdev);
			
		}

		return;
	}
	
	private static void calculateCluster1(Dev[] devices, Dev[] cluster, int umgebung) throws IOException {
		
		//devices[0] to devices[2] form an application
		Dev[] additional = new Dev[NUMOFDEVS];
		int numberclustermember = 0;
		
		double[] d = distanceTo(devices, 3, 0, 1, 2);
	//	printLocations(devices);
	//	System.out.println("Distances: " + d[0] + " " + d[1] + " " + d[2]);
		
		for(int i = 3; i < NUMOFDEVS; i++) {
			d = distanceTo(devices, i, 0, 1, 2);
			if(d[0] < umgebung || d[1] < umgebung || d[2] < umgebung) {
				if(compareIntervalls(devices, i, 0, 1, umgebung) || compareIntervalls(devices, i, 0, 2, umgebung) || compareIntervalls(devices, i, 1, 2, umgebung)) {
					cluster[i] = devices[i];
					numberclustermember++;
				}
			}
		}
		
		avgNumberOfClustermembers += numberclustermember;
		savetoCSV("./AdditionalClusterMembers.csv", cluster);
	return;
	}
	
	private static boolean compareIntervalls(Dev[] devices, int i, int ref1, int ref2, int umgebung) {
		if(((devices[i].x > Math.min(devices[ref1].x, devices[ref2].x)-umgebung) && (devices[i].x < Math.max(devices[ref1].x, devices[ref2].x)+umgebung)) && ((devices[i].y > Math.min(devices[ref1].y, devices[ref2].y)-umgebung) && (devices[i].y < Math.max(devices[ref1].y, devices[ref2].y)+umgebung))) {
			return true;
		}
			else {
				return false;
			}
	}
	
	private static double[] distanceTo(Dev[] devices, int dut, int a, int b, int c) {
		
		double[] returnval = new double[3];
		
		Point ab = new Point(), ac = new Point(), bc = new Point();
		ab.x = devices[1].x - devices[0].x;
		ab.y = devices[1].y - devices[0].y;
		ac.x = devices[2].x - devices[0].x;
		ac.y = devices[2].y - devices[0].y;
		bc.x = devices[2].x - devices[1].x;
		bc.y = devices[2].y - devices[1].y;
		
		
		returnval[0] = Math.abs((((devices[dut].x - devices[a].x)*(ab.y)) - ((ab.x)*(devices[dut].y - devices[a].y))) / (Math.sqrt((ab.x * ab.x) + (ab.y * ab.y))));
		returnval[1] = Math.abs((((devices[dut].x - devices[a].x)*(ac.y)) - ((ac.x)*(devices[dut].y - devices[a].y))) / (Math.sqrt((ac.x * ac.x) + (ac.y * ac.y))));
		returnval[2] = Math.abs((((devices[dut].x - devices[b].x)*(bc.y)) - ((bc.x)*(devices[dut].y - devices[b].y))) / (Math.sqrt((bc.x * bc.x) + (bc.y * bc.y))));
		
		return returnval;
		
	}

	private static void calculateCluster(Dev[] devices, Dev[] cluster) throws IOException {

		int numClustermember = 0;

		for (int i = 0; i < 3; i++) {
			cluster[i] = devices[i];
		}

		Dev[] additional = new Dev[NUMOFDEVS];
		int dist[] = new int[3];
		dist[0] = distance(cluster[0], cluster[1]);
		//dist[1] = distance(cluster[0], cluster[2]);
		dist[2] = distance(cluster[1], cluster[2]);

		
		 // System.out.printf("Distance 1 = %d\n", dist[0]);
		 // System.out.printf("Distance 2 = %d\n", dist[1]);
		 // System.out.printf("Distance 3 = %d\n", dist[2]);
		 
		Point halfs[] = new Point[2];

		halfs[0] = half(cluster[0], cluster[1]);
		//halfs[1] = half(cluster[0], cluster[2]);
		halfs[1] = half(cluster[1], cluster[2]);

		for (int i = 3; i < NUMOFDEVS; i++) {
			for (int j = 0; j < 2; j++) {
				if (distance(devices[i], halfs[j]) < 200) {

					numClustermember++;
					// System.out.printf("Addtional Clustermember : x = %d, y = %d\n", devices[i].x,
					// devices[i].y);
					additional[i] = devices[i];
					break;

				}
			}
		}

		avgNumberOfClustermembers += numClustermember;

		// System.out.print("Number of Clustermembers\t");
		// System.out.println(numClustermember);

		 savetoCSV("./AdditionalClusterMembers.csv",
		 additional);
		//savetoCSV("./AdditionalClusterMembers.csv", additional);
	}

	private static void placeDevs(Shape shape, Dev devices[]) throws IOException {

		switch (shape) {
		case square:
			for (int i = 0; i < Math.sqrt(NUMOFDEVS); i++) {
				for (int j = 0; j < Math.sqrt(NUMOFDEVS); j++) {
					devices[(int) (i * Math.sqrt(NUMOFDEVS) + j)] = new Dev(i, j);
				}
			}
			break;
		case linear:
			for (int i = 0; i < NUMOFDEVS; i++) {
				devices[i] = new Dev(i, 0);
			}
			break;
		case random:
			for (int i = 0; i < NUMOFDEVS; i++) {
				devices[i] = new Dev(generator.nextInt(area), generator.nextInt(area));
			}
			devices[0].x = 200;
			devices[0].y = 200;
			devices[1].x = area - 200;
			devices[1].y = 200;
			devices[2].x = area/2;
			devices[2].y = area - 200;
			
			savetoCSV("./allDevices.csv", devices);
			break;
		}
	}

	private static void printLocations(Dev devices[]) {
		for (int i = 0; i < NUMOFDEVS; i++) {
			System.out.printf("Dev %d: x = %d y = %d\n", i, devices[i].x, devices[i].y);
		}
	}

	private static void savetoCSV(String string, Dev devices[]) throws IOException {
		PrintWriter pw = new PrintWriter(string);
		pw.println("x;y");
		for (int i = 0; i < NUMOFDEVS; i++) {
			if (devices[i] != null)
				pw.printf("%d;%d\n", devices[i].x, devices[i].y);
		}
		pw.close();
		return;
	}

	
	private static void savetoCSV(String string, int values[][]) throws IOException {
		PrintWriter pw = new PrintWriter(string);
		pw.println("1000 nodes;2000 nodes;3000 nodes;4000 nodes;5000 nodes;6000 nodes;7000 nodes;8000 nodes;9000 nodes;10000 nodes;");
		
		for(int j = 0; j < ITERATIONS; j++) {
		for (int i = 0; i < STEPS; i++) {
				pw.printf("%d;", values[i][j]);
		}
		pw.printf("\n");
		}
		pw.close();
		return;
	}
	
	private static void savetoCSV(String string, int i[]) throws IOException {
		PrintWriter pw = new PrintWriter(string);
		pw.println("Umgebung;AvgNumDevices;");
		
		for(int j = 0; j < i.length; j++) {
		
			pw.printf("%d;%d;", j, i[j]);
		
		pw.printf("\n");
		}
		pw.close();
		return;
	}
	
	private static int distance(Dev dev1, Dev dev2) {
		return (int) Math.sqrt(((dev1.x - dev2.x) * (dev1.x - dev2.x)) + ((dev1.y - dev2.y) * (dev1.y - dev2.y)));
	}

	private static int distance(Dev dev1, Point p) {
		return distance(dev1, new Dev(p.x, p.y));
	}

	private static Point half(Dev dev1, Dev dev2) {
		Point p = new Point();
		p.x = (dev1.x + dev2.x)/2;
		p.y = (dev1.y + dev2.y)/2;
		//System.out.println("Dev 1 x = " + dev1.x + " y = " + dev1.y + " Dev 2 x = " + dev2.x + " y = " + dev2.y +  " Half is x: " + p.x + " y: " + p.y);
		return p;
	}

}
