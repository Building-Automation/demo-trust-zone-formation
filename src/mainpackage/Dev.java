package mainpackage;

public class Dev {

	enum ApplicationDomain {Light, DoorLockingSystem, Airconditioning};
	
	public int x, y;
	public boolean clusterMember = false;
	public ApplicationDomain domain;
	public int firmwareVersion;
	
	public Dev(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Dev(int x, int y, boolean groupMember) {
		this.x = x;
		this.y = y;
		this.clusterMember = groupMember;
	}
	
	public Dev(int x, int y, ApplicationDomain domain, int firmwareVersion) {
		this.x = x;
		this.y = y;
		this.domain = domain;
		this.firmwareVersion = firmwareVersion;
	}
	
}
